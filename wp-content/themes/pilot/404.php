<?php get_header(); ?>
<?php get_all_blocks('sidebar-menu', true); ?>

<div class="post-wrapper">
	<header class="page-header">
		<h1 class="page-title">404 Page Not Found</h1>
		<h4>We're not sure where it went. Try a search, or selecting something from the sidebar menu!</h4>
	</header>
</div>

<?php
get_footer();
