<?php get_header(); ?>
	<?php if ( have_posts() ) : ?>

		<header class="page-header">
			<h1 class="page-title">All Forms</h1>
		</header>
		<ul>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php 
					global $pilot;
					$slug = $pilot->language;
					$files = get_field( $slug.'_files');
					$file = $files[0][$slug.'_upload'];
	//print_r($file);
					$filename = benefits_title( $file['ID']);

				?>
				<li><a href="<?php echo get_permalink($file['ID']); ?>" download><?php echo $filename; ?></a></li>
			<?php endwhile; ?>
		</ul>
	<?php endif; ?>
<?php get_footer(); ?>