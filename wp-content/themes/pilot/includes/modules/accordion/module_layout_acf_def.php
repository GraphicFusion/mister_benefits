<?php
	global $pilot;
	// add module layout to flexible content 
	$fields = [];
	foreach( $pilot->languages as $name => $slug ){
		$fields[] = array (
			'key' => create_key('lang',$slug),
			'label' => $name,
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		);
		$fields[] = 
			array (
				'key' => 'field_5696bca67b7cc'.$slug,
				'label' => 'Content',
				'name' => $slug.'_accordion_block_content',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			);
		$fields[] = array (
				'key' => 'field_5696bcd27b7cd'.$slug,
				'label' => 'Accordion Rows',
				'name' => $slug.'_accordion_block_rows',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => '',
				'max' => '',
				'layout' => 'table',
				'button_label' => 'Add Row',
				'sub_fields' => array (
					array (
						'key' => 'field_5696be227b7ce'.$slug,
						'label' => 'Lede',
						'name' => 'lede',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_5696be2a7b7cf'.$slug,
						'label' => 'Hidden Content',
						'name' => 'hidden_content',
						'type' => 'wysiwyg',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'tabs' => 'all',
						'toolbar' => 'full',
						'media_upload' => 1,
					),
				),
			);

	}
	// add module layout to flexible content 
	$module_layout = array (
		'key' => '5696bc8b7b7ca',
		'name' => 'accordion_block',
		'label' => 'Accordion Block',
		'display' => 'block',
		'sub_fields' => $fields,
		'min' => '',
		'max' => '',
	);
?>