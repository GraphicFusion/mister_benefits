<?php
	$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}

	function build_menu_layout(){
		global $pilot;
		$slug = $pilot->language;
		$rows = get_sub_field($slug.'_links');
		$current_parent = [];
		$current_url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

		foreach( $rows as $row ){
			if($current_url == $row['link']){ 
				$row['class'] = " active"; 
			}
			if($row['indent']){
				if($current_url == $row['link']){ 
					$current_parent['class'] = "menu-open active";
				}
				$current_parent['children'][] = $row;
			}
			else{
				if( count( $current_parent ) > 0 ){
					$list[] = $current_parent;
				}
				$current_parent = $row;
				$current_parent['children'] = [];				
			}
		}
		$list[] = $current_parent;
		$args = array(
			'title' => get_sub_field('menu_block_title'),
			'rows' => $list
		);
		return $args;
	}
?>