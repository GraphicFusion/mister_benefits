<?php 
	/**
	 * string	$args['title']
	 * array	$args['rows']			 //  array of rows of lede/content
	 * string	$args['rows'][0]['text'] //  row title
	 * string	$args['rows'][0]['link'] //  
	 * string	$args['rows'][0]['override'] //  
	 * string	$args['rows'][0]['children'] // sub links (one level only)  
	 * string	$args['rows'][0]['children']['rows'][0]['text'] 
	 * string	$args['rows'][0]['children']['rows'][0]['link'] 
	 * string	$args['rows'][0]['children']['rows'][0]['override'] 
	 */
	global $args; 
?>
<?php if( is_array($args['rows']) && count($args['rows']) > 0 ) : ?>
	<?php if( $args['title'] ) : ?>
		<h3><?php echo $args['title']; ?></h3>
	<?php endif; ?>
	<div class='mob-menu-btn'></div>
	<div class="overlay"></div>
	<ul>
		<?php if( count($args['rows']) > 0 ) : ?>
			<?php foreach( $args['rows'] as $row ): 
					$class = "";
					if( array_key_exists('class', $row) ){ $class = $row['class']; }
			?>
				<li class="<?php echo $class; ?>">
					<?php if( $row['override'] ){ $link = $row['override']; }else{ $link = $row['link']; } ?>
					<a href="<?php echo $link; ?>">
						<?php echo $row['text']; ?>
					</a>
					<?php if( count( $row['children'])>0 ): ?>
						<a href="#" class="menu-btn" aria-label="expand/hide submenu"></a>
						<ul class="sub-menu">
						<?php foreach( $row['children'] as $sub ): 
								$subclass = "";
								if( array_key_exists('class', $sub) ){ $subclass = $sub['class']; }
						?>
							<li class="<?php echo $subclass; ?>">
								<?php if( $sub['override'] ){ $link = $sub['override']; }else{ $link = $sub['link']; } ?>
								<a href="<?php echo $link; ?>">
									<?php echo $sub['text']; ?>
								</a>
							</li>
						<?php endforeach; ?>
						</ul>				
					<?php endif; ?>
				</li>
			<?php endforeach; ?>
		<?php endif; ?>
	</ul>
<?php endif; ?>