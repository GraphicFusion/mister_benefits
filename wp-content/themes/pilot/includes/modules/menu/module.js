jQuery(document).ready(function($) {
	$('.menu-btn').on('click', function() {
		$(this).closest('li').toggleClass('menu-open');
		$(this).next('.sub-menu').slideToggle(250);
	});

	$('.block-menu .active .sub-menu').slideToggle();

	var menu = $('.block-menu');

	$('.mob-menu-btn').on('click', function () {
		if ( menu.css('left') == '0px' ) {
			menu.css('left', '-300px');
		} else {
			menu.css('left', '0px' );
			$('.block-menu').children('a').first().focus();
		}
	});

	$('.block-menu').on('focusout', function () {
		console.log('focusout');
		if ( menu.css('left') == '0px' ) {
			menu.css('left', '-300px');
		}
	});

	$('.block-menu').on('focusin', function() {
		console.log('focusin');
	});

	$(window).on('resize', function() {
		if ( $(window).width() > 678 ) {
			menu.css('left', 'auto');
		} else {
			menu.css('left', '-300px');
		}
	});
});