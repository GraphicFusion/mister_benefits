<?php
	global $pilot;
	// add module layout to flexible content 
	$fields = [];
	foreach( $pilot->languages as $name => $slug ){
		$fields[] = array (
			'key' => create_key('lang',$slug),
			'label' => $name,
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		);
		$fields[] = 			array (
				'key' => create_key('menu',$slug.'_links'),
				'label' => $name . ' Links',
				'name' => $slug.'_links',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => '',
				'max' => '',
				'layout' => 'table',
				'button_label' => 'Add Menu Item',
				'sub_fields' => array (
					array (
						'key' => create_key( 'menu', 'Text'),
						'label' => 'Text',
						'name' => 'text',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => create_key( 'menu', 'link'),
						'label' => 'Link',
						'name' => 'link',
						'type' => 'page_link',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
					),
					array (
						'key' => create_key( 'menu', 'override'),
						'label' => 'Override',
						'name' => 'override',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
					),
					array (
						'key' => create_key( 'menu', 'indent'),
						'label' => 'Indent',
						'name' => 'indent',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
					),
				),
			);

	}
	$module_layout = array (
		'key' => create_key('menu'),
		'name' => 'menu',
		'label' => 'Menu',
		'display' => 'block',
		'sub_fields' => $fields,
		'min' => '',
		'max' => '',
	);
?>