<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if( file_exists ( $filename )){
    require $filename;
}
function limit_text($content, $len){
	if( strlen( $content) < $len ){
		return $content;
	}
	$pos=strpos($content, ' ', $len);
	return substr($content,0,$pos ); 
}

function build_post_slides_layout(){
	global $pilot;
	$slug = $pilot->language;
	$count = get_sub_field('post_slides_count');
	if( !$count ){
		$count = 3;
	}
	$posts = wp_get_recent_posts( array('numberposts' => $count ) );
	if( count($posts) > 0  ){
		$posts_arr = [];
		foreach( $posts as $post ){
			setup_postdata($post);
			$id = $post['ID'];
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'large' );
			$posts_arr[] = array(
				'image' => $image[0],
				'post_title' => benefits_title( $id),
				'content' => get_field($slug.'_excerpt', $id),
				'permalink' => get_permalink( $id)
			);
		}
	    $args = array(
			'title' => get_sub_field('post_slides_block_title'),
	        'posts' => $posts_arr
	    );
	    return $args;
	}
}
?>