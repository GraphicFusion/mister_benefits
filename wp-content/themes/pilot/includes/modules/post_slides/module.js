/* Multi Post Slider*/
jQuery(document).ready(function($) {
	
	$('.block-post_slides .slider-block').slick({
		easing: 'swing',
		arrows: true,
		autoplay: false,
		nextArrow: "<button class='slick-next'></button>",
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplaySpeed: 3000,
		infinite: true,
		responsive: [

			{
				breakpoint: 600,
				settings: {
				}
			}
		]
	});
	
});

