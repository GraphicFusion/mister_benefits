<?php
global $pilot;
// add module layout to flexible content
$module = 'post_slides';
$module_layout = array(
    'key' => create_key($module),
    'name' => 'post_slides_block',
    'label' => 'Post Slides Block',
    'display' => 'block',
		'sub_fields' => array (
			array (
				'key' => 'field_598ew98t0wr232',
				'label' => 'Number of Posts',
				'name' => 'post_slides_count',
				'type' => 'number',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
		),
    'min' => '',
    'max' => '',
);

?>