<?php
/* 
 * Options Page
 * Add ACF items to an options page 
 * */
global $pilot;
if ( function_exists('acf_add_options_page') ) {

    acf_add_options_page(
        array(
            'page_title'    => 'Site Options',
            'menu_title'    => 'Site Options',
            'menu_slug'     => 'site-options',
            'capability'    => 'edit_posts',
            'parent_slug'   => '',
            'position'      => false,
            'icon_url'      => false
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title'    => 'Theme Settings',
            'menu_title'    => 'Theme Settings',
            'menu_slug'     => 'theme-settings-header',
            'capability'    => 'edit_posts',
            'parent_slug'   => 'site-options',
            'position'      => false,
            'icon_url'      => false
        )
    );    
    acf_add_options_sub_page(
        array(
            'page_title'    => 'Sidebar Menu',
            'menu_title'    => 'Sidebar Menu',
            'menu_slug'     => 'menu-page',
            'capability'    => 'edit_posts',
            'parent_slug'   => 'site-options',
            'position'      => false,
            'icon_url'      => false
        )
    );    
/*
    acf_add_options_sub_page(
        array(
            'page_title'    => 'Header',
            'menu_title'    => 'Header',
            'menu_slug'     => 'site-options-header',
            'capability'    => 'edit_posts',
            'parent_slug'   => 'site-options',
            'position'      => false,
            'icon_url'      => false
        )
    );    
    acf_add_options_sub_page(
        array(
            'page_title'    => 'Footer',
            'menu_title'    => 'Footer',
            'menu_slug'     => 'site-options-footer',
            'capability'    => 'edit_posts',
            'parent_slug'   => 'site-options',
            'position'      => false,
            'icon_url'      => false
        )
	);
*/
}
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5847177177c0e',
	'title' => 'Sitewide Setting',
	'fields' => array (
		array (
			'key' => 'field_584717797fa03',
			'label' => 'Default Background Image',
			'name' => 'default_background',
			'type' => 'file',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'library' => 'all',
			'min_size' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'theme-settings-header',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

if( function_exists('acf_add_local_field_group') ):
	$fields = array();
$fields[] = 		array (
			'default_value' => 0,
			'message' => '',
			'ui' => 0,
			'ui_on_text' => '',
			'ui_off_text' => '',
			'key' => 'field_584f266cadd14',
			'label' => 'Active',
			'name' => 'alert_active',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
		);

	foreach( $pilot->languages as $name => $slug ){
		$fields[] = array (
			'placement' => 'top',
			'endpoint' => 0,
			'key' => 'field_584f25a3add12'.$slug,
			'label' => $name,
			'name' => $slug."_tab",
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
		);
		$fields[] = array (
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 1,
			'default_value' => '',
			'delay' => 0,
			'key' => 'field_584f2656add13'.$slug,
			'label' => 'Alert',
			'name' => $slug.'_alert',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
		);
	}
acf_add_local_field_group(array (
	'key' => 'group_584f259214de2',
	'title' => 'Alert',
	'fields' => $fields,
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'theme-settings-header',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;
?>