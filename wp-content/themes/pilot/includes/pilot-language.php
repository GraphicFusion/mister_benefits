<?php
//build_language_acf();
	function create_lang_key($module=null,$name=null){
		return 'field_' . hash('md5', $module."_block".$name); 	
	}
	add_action( 'wp_loaded', 'build_language_acf', 1 );
	function build_language_acf(){
		global $pilot;
		$fields = [];
		$languages = $pilot->languages;
		$is_benefits = 0;
		if( is_admin() ){
			if( array_key_exists('post_type', $_GET) && $_GET['post_type'] == 'benefits_form' ){
				$is_benefits = 1;
			}
			if( array_key_exists('post', $_GET) && is_admin() ){
				$post = get_post( $_GET['post'] );
				if( 'benefits_form' == $post->post_type ){
					$is_benefits = 1;
				}
			}
		} 
		if( function_exists('acf_add_local_field_group') ):
			if( !$is_benefits ) {
				$fields[] = array (
					'key' => create_key('lang','associated_files'),
					'label' => 'Associated Files',
					'name' => 'associated_files',
					'type' => 'repeater',
					'instructions' => 'A "Benefits Form" will have versions saved for each language and the appropriate language will be shown on the web page. <a href="/wp-admin/post-new.php?post_type=benefits_form">Add a new form or downloadable file here.</a>',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => '',
					'min' => '',
					'max' => '',
					'layout' => 'table',
					'button_label' => 'Add a File/Form',
					'sub_fields' => array (
						array (
							'key' => create_key('lang','file'),
							'label' => 'File',
							'name' => 'file',
							'type' => 'post_object',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'post_type' => array (
								0 => 'benefits_form',
							),
							'taxonomy' => array (
							),
							'allow_null' => 0,
							'multiple' => 0,
							'return_format' => 'object',
							'ui' => 1,
						),
					),
				);
				$fields[] = array (
					'key' => create_key('lang','associated_links'),
					'label' => 'Associated Links',
					'name' => 'associated_links',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => '',
					'min' => '',
					'max' => '',
					'layout' => 'block',
					'button_label' => 'Add a Link',
					'sub_fields' => array (
/*						array (
							'key' => create_key('lang','link_text'),
							'label' => 'Link Text',
							'name' => 'link_text',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '30%',
								'class' => '',
								'id' => '',
							),
							'allow_null' => 0,
							'multiple' => 0,
							'ui' => 1,
						),
*/
						array (
							'key' => create_key('lang','link'),
							'label' => 'Link',
							'name' => 'link',
							'type' => 'post_object',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'post_type' => array (
							),
							'taxonomy' => array (
							),
							'allow_null' => 0,
							'multiple' => 0,
							'return_format' => 'object',
							'ui' => 1,
						),
/*
						array (
							'key' => create_key('lang','custom_link'),
							'label' => 'Custom Link',
							'name' => 'custom_link',
							'type' => 'text',
							'instructions' => 'For example: www.google.com, which will override any link to the left.',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '40%',
								'class' => '',
								'id' => '',
							),
							'allow_null' => 0,
							'multiple' => 0,
							'ui' => 1,
						),
*/
					),

				);
		
			}
			foreach( $languages as $name => $slug ){
				$fields[] = array (
					'key' => create_lang_key('lang',$slug),
					'label' => $name,
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'top',
					'endpoint' => 0,
				);
				$fields[] =	array (
					'key' => create_key('lang',$slug.'_title'),
					'label' => $name.' Title',
					'name' => $slug.'_title',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				);
				
				if( (array_key_exists('post_type', $_GET) && $_GET['post_type'] == 'benefits_form' ) 
					|| !is_admin()
					|| $is_benefits || count($_POST) > 0) {
					$fields[] = array (
						'key' => create_lang_key('lang', $slug.'_files'),
						'label' => $name.' File',
						'name' => $slug.'_files',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '1',
						'max' => '1',
						'layout' => 'table',
						'button_label' => 'Add Row',
						'sub_fields' => array (
							array (
								'key' => create_lang_key('lang',$slug.'_upload'),
								'label' => $name.' File',
								'name' => $slug.'_upload',
								'type' => 'file',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'return_format' => 'array',
								'library' => 'all',
								'min_size' => '',
								'max_size' => '',
								'mime_types' => '',
							),
							array (
								'key' => create_lang_key('lang', $slug.'_active_date'),
								'label' => 'Active Date',
								'name' => $slug.'_active_date',
								'type' => 'date_picker',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'display_format' => 'd/m/Y',
								'return_format' => 'd/m/Y',
								'first_day' => 1,
							),
							array (
								'key' => create_lang_key('lang', $slug.'_inactive_date'),
								'label' => 'Inactive Date',
								'name' => $slug.'_inactive_date',
								'type' => 'date_picker',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'display_format' => 'd/m/Y',
								'return_format' => 'd/m/Y',
								'first_day' => 1,
							),
						),
					);
				}
				$fields[] = array (
					'key' => create_lang_key('lang',$slug.'_content'),
					'label' => $name.' Content',
					'name' => $slug.'_content',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
				);
				$fields[] = array (
					'key' => create_lang_key('lang',$slug.'_excerpt'),
					'label' => $name.' Excerpt',
					'name' => $slug.'_excerpt',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
				);
			}
acf_add_local_field_group(array (
	'key' => 'group_58371c0740066',
	'title' => 'Content',
	'fields' => $fields,
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'benefits_form',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));
		endif;
	}
	function set_benefits_language( $lang = null ){
		if(!$lang){
			$lang = "eng";
		}
		$expire = time()+60*60*24*30;
		setcookie('benefits-lang', $lang, $expire, "/");
	}
	function get_benefits_language(){
		global $pilot;
		if(isset($_COOKIE['benefits-lang'])) {
			$lang = $_COOKIE['benefits-lang'];
		}
		else{
			$lang = "eng";
		}
		if( !property_exists( $pilot, 'language')){
			$pilot->language = $lang;
		}
	}
	add_action( 'init', 'process_language_form', 1 );
	add_action( 'init', 'get_benefits_language', 9 );
	function process_language_form() {
		global $pilot;
	    if( isset( $_POST['lang_nonce'] ) ) {
		 	if ( check_admin_referer( 'lang_nonce_save', 'lang_nonce' )){
				
				$pilot->language = "eng";
				if( array_key_exists( 'benefits_language', $_POST ) ){
					$pilot->language = "esp";
				}
				set_benefits_language( $pilot->language );
				global $wp;
				$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				wp_redirect($url);
				exit;
		    }else{
		        wp_die('Security check fail'); 
		    }
	     }
	}

	// disale native title and editor
	add_action('admin_init', 'disable_benefits_support');
	function disable_benefits_support() {
		remove_post_type_support( 'post', 'editor' );
		remove_post_type_support( 'benefits_form', 'editor' );
		remove_post_type_support( 'page', 'editor' );
	}
	function benefits_title( $post_id ){
		global $pilot;
		$slug = $pilot->language;
		$lang_title = get_field($slug.'_title', $post_id);
		if( !$lang_title ){
			$lang_title = get_the_title( $post_id );
		}
		return $lang_title;
	}
	add_filter('relevanssi_excerpt_content', 'custom_fields_to_excerpts', 10, 3);
	function custom_fields_to_excerpts($content, $post, $query) {
		$content .= " " . get_field('eng_content', $post->ID);
		$content .= " " . get_field('esp_content', $post->ID);
		return $content;
	}
?>