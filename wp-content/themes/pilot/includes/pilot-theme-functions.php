<?php
	load_theme_textdomain( 'pilot', get_template_directory_uri().'/languages' );
	function pilot_get_title(){
		if (is_home()) {
			if (get_option('page_for_posts', true)) {
				return get_the_title(get_option('page_for_posts', true));
			}
			else {
				return __('Latest Posts', 'dorado');
			}
		} elseif (is_archive()) {
			return get_the_archive_title();
		}
		elseif (is_search()) {
			return sprintf(__('Search Results for %s', 'dorado'), get_search_query());
		}
		elseif (is_404()) {
			return __('Not Found', 'dorado');
		}
		else {
			return get_the_title();
		}
	}
	function pilot_get_view_format(){
		return;
	}
	function pilot_get_sidebar(){
		global $pilot;
		if( $pilot->sidebar ){
			get_sidebar();
		}
	}
	function pilot_get_comments(){
		global $pilot;
		if( $pilot->comments ){
			if ( comments_open() || get_comments_number() ){
				comments_template();
			}			
		}
	}
	function acf_load_cdn_field_choices( $field ) {
	    $field['choices'] = array();
		$type = get_field('theme_type', 'option');
		$gender = get_field('theme_gender', 'option');
		$url = 'http://fitmaster.wpengine.com/wp-json/wp/v2/media?filter[media_category]='.$type.'%2B'.$gender;
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		$media = json_decode($output);
		foreach( $media as $media ){
			$url = $media->source_url;
			$field['choices'][$url] = "<img width='100px' src='".$url."'>";
		}
		curl_close($ch);    
		return $field;   
	}
	add_filter('acf/load_field/name=cdn_image', 'acf_load_cdn_field_choices');
	
	function asset_path($filename) {
		$dist_path = get_template_directory_uri() . DIST_DIR;
		$directory = dirname($filename) . '/';
		$file = basename($filename);
		static $manifest;
		
		if (empty($manifest)) {
			$manifest_path = get_template_directory() . DIST_DIR . 'assets.json';
			$manifest = new JsonManifest($manifest_path);
		}
		if (array_key_exists($file, $manifest->get())) {
			return $dist_path . $directory . $manifest->get().array($file);
		} else {
			return $dist_path . $directory . $file;
		}
	}
	function pilot_breadcrumbs() { ?>

			<div class="bc-home"><a href="<?php echo get_option('home'); ?>"></a></div>

			<?php if (is_category() || is_single()) : ?>


				<?php if (is_single()) : ?>

					<div class="bc-item"><?php echo the_title(); ?></div>

				<?php endif; ?>

			<?php elseif (is_page() && ( !is_home() && !is_front_page()) ) :
					global $post;
					$parent_id = $post->post_parent;
					if( $parent_id ) : ?>
						<div class="bc-item"><a href="<?php echo get_permalink($parent_id); ?>"><?php echo benefits_title( $parent_id ); ?></a></div>
					<?php endif; ?>
					<div class="bc-item"><a href="<?php echo get_permalink($post); ?>"><?php echo benefits_title( $post->ID ); ?></a></div>
			<?php endif; ?>
<?php
	}
	function display_breadcrumbs() {
	?><div class="breadcrumbs"><?php pilot_breadcrumbs(); ?></div><?php
	}
	
function custom_menu_page_removing() {
	unregister_nav_menu( 'primary' );
    remove_submenu_page( 'themes.php','nav-menus.php' );
}
add_action( 'admin_menu', 'custom_menu_page_removing' );



?>