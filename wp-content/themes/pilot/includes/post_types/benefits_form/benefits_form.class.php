<?php
namespace Mister;
class benefits_form{
	public function __construct( $benefits_form_ref = null ){
		global $pilot;
		if( is_numeric( $benefits_form_ref ) ){
			$benefits_form = get_benefits_form_by( 'ID', $benefits_form_ref );
		}
		elseif( is_object( $benefits_form_ref ) ){
			$benefits_form = $benefits_form_ref;
		}
		else{
			$benefits_form = "";
		}
		if( is_object( $benefits_form ) ){
			foreach( $benefits_form as $key => $value ){
				$this->$key = $value;
			}
		}
		$this->get_benefits_form_meta();
		$this->wp_benefits_form = $benefits_form;
	}
	public function get_benefits_form_meta(){
		global $pilot;
		$slug = $pilot->language;
		$files = get_field( $slug.'_files', $this->ID );
		if( is_array( $files ) ){
			$index = count($files) - 1;
			$this->file = $files[$index][$slug.'_file'];
		}

	}

}