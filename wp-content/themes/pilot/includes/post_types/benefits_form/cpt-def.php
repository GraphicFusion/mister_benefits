<?php
/**
 * Form Custom Post Type
 */
add_action( 'init', 'register_benefits_forms_post_type');
function register_benefits_forms_post_type()
{ 
	register_post_type( 'benefits_form',
		array( 'labels' => 
			array(
				'name'               => 'Benefits Forms',
				'singular_name'      => 'Benefits Form',
				'all_items'          => 'All Forms',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Benefits Form',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Form',
				'new_item'           => 'New Form',
				'view_item'          => 'View Form',
				'search_items'       => 'Search Form',
				'not_found'          => 'Nothing found in the Database.',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon'  => ''
			),
			'description'         => 'Form post type',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 14,
			'supports'      => array( 'benefits_form','title', 'benefits_form', 'thumbnail','editor'),
			'menu_icon'           => 'dashicons-id',
			 'rewrite'	      => array( 'slug' => 'benefits_form', 'with_front' => false ),
			 'has_archive'      => 'benefits_form',
		)
	);
}
?>