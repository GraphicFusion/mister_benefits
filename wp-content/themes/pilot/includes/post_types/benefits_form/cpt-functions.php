<?php
	require get_template_directory() . '/includes/post_types/benefits_form/benefits_form.class.php';
	require get_template_directory() . '/includes/post_types/benefits_form/cpt-acf.php';
	require get_template_directory() . '/includes/post_types/benefits_form/cpt-def.php';

function get_all_benefits_forms(){
	$benefits_forms = array();
	$args=array(
	  'post_type' => 'benefits_form',
	  'post_status' => 'publish',
	  'posts_per_page' => -1
	);
	$auth_query = new WP_Query($args);
	if( $auth_query->have_posts() ):
		foreach($auth_query->posts as $auth_post):
			$benefits_forms[] = new benefits_form($auth_post);			
		endforeach;
	endif;
	return $benefits_forms;
}
function mister_benefits_form( $benefits_form_ref = null ){
	$benefits_form = new Mister\benefits_form( $benefits_form_ref );
	return $benefits_form;
}