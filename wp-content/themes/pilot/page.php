<?php get_header(); ?>

		<?php while ( have_posts() ) : the_post(); ?>
			<?php if(is_home() || is_front_page() ) : ?>
				<?php 	get_all_blocks('above-content', true); // defined in /inc/content-blocks.php ?>
			<?php endif; ?>
			<?php get_template_part( 'views/content', 'page' ); ?>
			<?php pilot_get_comments(); ?>
		<?php endwhile; ?>

<?php get_footer(); ?>