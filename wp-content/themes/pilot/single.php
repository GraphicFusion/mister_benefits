<?php get_header(); ?>
<?php get_all_blocks(); ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php 
				$type = $post->post_type;
			?>
			<?php get_template_part( 'views/content' ); ?>
			<?php if( !is_home() && !is_front_page() ) : ?>
				<?php get_all_blocks('below-content',true); // defined in /inc/content-blocks.php ?>
			<?php endif; ?>

		<?php endwhile; ?>

<?php get_footer(); ?>