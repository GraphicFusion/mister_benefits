(function() {

    wow = new WOW({
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       0,          // default
        mobile:       true,       // default
        live:         true        // default
    });
    wow.init();

})();
jQuery(document).ready(function($) {
    console.log( "Your JS is ready" );

	$('#language-toggle input').change(function(e) {
		$('form#language-toggle').submit();
	});

    $('.scroll-top').on('click', function(e) {
        e.preventDefault();
        $('body,html').animate({
            scrollTop : 0
        }, 500);
    });

    $('.close').on('click', function() {
        $('.alert').slideUp();
    })
});