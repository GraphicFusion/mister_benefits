<?php global $pilot; ?>
<?php
	$img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
	if( is_array( $img ) ){
		$src = $img[0];
	}
	else{
		$default = get_field('default_background','option');
		$src = $default['url'];
	}
 ?>
<section>
	<div>
		<a href="<?php echo get_permalink(); ?>"><h3><?php echo benefits_title( $post->ID ); ?></h3></a>
		<span>Updated <?php echo get_the_modified_date('m.d.Y'); ?></span>
	</div>	
</section>