<?php global $pilot; ?>
<?php get_all_blocks('sidebar-menu', true); ?>
	<?php
		$img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
		if( is_array( $img ) ){
			$src = $img[0];
		}
		else{
			$default = get_field('default_background','option');
			$src = $default['url'];
		}
	 ?>
<header class="entry-header"><!--featured image-->
	<div class="header-meta">
		<h1 class="entry-title"><?php echo benefits_title( $post->ID ); ?></h1>
		<span>Updated <?php echo get_the_modified_date('m.d.Y'); ?></span>
	</div>	
</header>
<div class="page">
<?php
	$slug = $pilot->language;
?>
	<div class="files-and-links"><!--files-->
		<ul>
<?php	
						$lang_file_title = get_field($slug.'_title');
						if( !$lang_file_title ){
							$lang_file_title = get_field('eng_title');
						}
						$lang_files = get_field($slug.'_files');
						if( !$lang_files ){
							$lang_files = get_field('eng_files');
						}
						if( count( $lang_files ) > 0 ) : 
							$curr_file = $lang_files[0][$slug.'_upload'];
							echo "<li><a class='file' href='".$curr_file['url']."' download>".$lang_file_title."</a></li>";
						endif; 
				?>
		</ul>
	</div>
</div>

