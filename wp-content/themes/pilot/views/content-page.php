<?php global $pilot; ?>
<?php get_all_blocks('sidebar-menu', true); ?>
	<?php
		$img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
		if( is_array( $img ) ){
			$src = $img[0];
		}
		else{
			$default = get_field('default_background','option');
			$src = $default['url'];
		}
	 ?>
<header class="entry-header"><!--featured image-->
	<img src="<?php echo $src; ?>" alt="<?php echo benefits_title( $post->ID ); ?>">
	<div class="header-meta">
		<h1 class="entry-title"><?php echo benefits_title( $post->ID ); ?></h1>
		<span>Updated <?php echo get_the_modified_date('m.d.Y'); ?></span>
	</div>	
</header>
<?php
	$slug = $pilot->language;
	$files = get_field( 'associated_files');
	$links = get_field( 'associated_links');
//print_r($links);
	if( (is_array($files) && count($files)>0) || (is_array($links) && count($links)>0)): ?>
	<div class="files-and-links"><!--associated-files-and-links-->
		<ul>
			<?php if( is_array($files) && count($files)>0) : ?>
				<?php foreach( $files as $file ) : 
					if( is_object( $file['file'] ) ){
						$lang_file_title = get_field($slug.'_title', $file['file']->ID);
						if( !$lang_file_title ){
							$lang_file_title = get_field('eng_title', $file['file']->ID);
						}
						$lang_files = get_field($slug.'_files', $file['file']->ID);
						if( !$lang_files ){
							$lang_files = get_field('eng_files', $file['file']->ID);
						}
						if( count( $lang_files ) > 0 ) : 
							$curr_file = $lang_files[0][$slug.'_upload'];
							echo "<li><a class='file' href='".$curr_file['url']."' download>".$lang_file_title."</a></li>";
						endif; 
					}
				?>
				<?php endforeach; ?>
			<?php endif; ?>
			<?php if( is_array($links) && count($links)>0) : ?>
				<?php foreach( $links as $link_arr ) : $link = $link_arr['link']; 
					
					echo "<li><a class='page' href='".get_permalink( $link->ID )."'>".$link->post_title."</a></li>";
				?>
				<?php endforeach; ?>
			<?php endif; ?>
		</ul>
	</div>
<?php endif; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php echo get_field( $slug.'_content'); ?>
		<?php 
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'pilot' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<footer class="entry-footer">
<?php get_all_blocks('footer-content'); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'pilot' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer>
</article>
<?php get_all_blocks('content'); // defined in /inc/content-blocks.php ?>
<?php if( !is_home() && !is_front_page() ) : ?>
	<?php get_all_blocks('below-content',true); // defined in /inc/content-blocks.php ?>
<?php endif; ?>