<?php global $pilot; ?>
<?php get_all_blocks('sidebar-menu', true); ?>

<div class="post-wrapper">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header >
				<a href="<?php echo get_permalink(); ?>"><h2><?php echo benefits_title($post->ID); ?></h2></a>		
			</header><!-- .entry-header -->
		<div class="entry-summary">
			<?php 
				$type = get_post_type(); 
				if( 'page' == $type || 'post' == $type || 'benefits_form' == $type ) :
					// get associated file info if any 
					$slug = $pilot->language;
					$files = get_field('associated_files');
					if( is_array( $files ) ): ?>
						<ul>
						<?php foreach( $files as $file ) : 
							$filename = get_field($slug.'_title', $file['file']->ID); ?>
							<li>File: <?php echo $filename; ?></li>
						<?php endforeach; ?>
						</ul>
				<?php endif; ?>
			<?php endif; ?>
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
	</article><!-- #post-## -->
</div>