<?php global $pilot; ?>
<?php get_all_blocks('sidebar-menu', true); ?>
	<?php
		$img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
		if( is_array( $img ) ){
			$src = $img[0];
		}
		else{
			$default = get_field('default_background','option');
			$src = $default['url'];
		}
	 ?>
<header class="entry-header"><!--featured image-->
	<div class="header-meta">
		<span>Updated <?php echo get_the_modified_date('m.d.Y'); ?></span>
		<h1 class="entry-title"><?php echo benefits_title( $post->ID ); ?></h1>
	</div>	
	<img src="<?php echo $src; ?>" alt="<?php echo benefits_title( $post->ID ); ?>">
</header>
<div class="page">
<?php
$slug = $pilot->language;
	$files = get_field( 'associated_files');
	$links = get_field( 'associated_links');
//print_r($links);
	if( (is_array($files) && count($files)>0) || (is_array($links) && count($links)>0)): ?>
	<div class="files-and-links"><!--associated-files-and-links-->
		<ul>
			<?php if( is_array($files) && count($files)>0) : ?>
				<?php foreach( $files as $file ) : 
					if( is_object( $file['file'] ) ){
						$lang_file_title = get_field($slug.'_title', $file['file']->ID);
						if( !$lang_file_title ){
							$lang_file_title = get_field('eng_title', $file['file']->ID);
						}
						$lang_files = get_field($slug.'_files', $file['file']->ID);
						if( !$lang_files ){
							$lang_files = get_field('eng_files', $file['file']->ID);
						}
						if( count( $lang_files ) > 0 ) : 
							$curr_file = $lang_files[0][$slug.'_upload'];
							echo "<li><a class='file' href='".$curr_file['url']."' download>".$lang_file_title."</a></li>";
						endif; 
					}
				?>
				<?php endforeach; ?>
			<?php endif; ?>
			<?php if( is_array($links) && count($links)>0) : ?>
				<?php foreach( $links as $link_arr ) : $link = $link_arr['link']; 
					
					echo "<li><a class='page' href='".get_permalink( $link->ID )."'>".$link->post_title."</a></li>";
				?>
				<?php endforeach; ?>
			<?php endif; ?>
		</ul>
	</div>
</div>
<?php endif; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if( $pilot->use_default_page_titles && !get_field('hide_title') ) : ?>
		<header class="entry-header">
			<?php if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php pilot_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php
			endif; ?>
		</header><!-- .entry-header -->
	<?php endif; ?>
	<div class="entry-content">
		<?php echo get_field( $pilot->language."_content" ); ?>		
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php //pilot_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->