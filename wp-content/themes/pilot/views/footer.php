<footer class="site-footer">
	<div class="layout-content">
		<a href="<?php echo get_site_url(); ?>"><img src="<?php echo get_site_url(); ?>/wp-content/themes/pilot/image/mister-logo.png" /></a>
		<a class="scroll-top" href="#" title="Back to Top"></a>
		<div class="site-info">
			<div class="copyright">
				&copy; Copyrighted Car Wash Partners, Inc. <?php echo date("Y"); ?>. All Rights Reserved.
			</div>
			<?php printf( esc_html__( 'Design by %1$s', 'pilot' ), '<a href="http://sonderagency.com/">Sonder</a>' ); ?> &bull; 
			<a href="#">Privacy Policy</a> &bull; <a href="#">Terms of Use</a>
		</div><!-- .site-info -->
	</div>
</footer>