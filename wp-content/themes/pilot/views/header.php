<header id="masthead" class="site-header">
    <div class="btn-wrap"><div class="mob-menu-btn"></div></div>
    <div class="top_section">
        <div class="site-branding">
            <h1><a href="<?php echo get_site_url(); ?>"><img src="<?php echo get_site_url(); ?>/wp-content/themes/pilot/image/mister-logo.png" />Benefits</a></h1>
        </div><!-- .site-branding -->

        <div class="header-utilities">
            <div class="controls">
                <?php get_template_part( 'views/language', 'toggle' ); ?>
                <?php get_search_form(); ?>
            </div>
            <?php display_breadcrumbs() ?>
        </div>
    </div>
</header>
<?php
	global $pilot;
	$lang = $pilot->language;
	if( get_field('alert_active','option') ) :
		$alert = get_field($lang.'_alert','option');
?>
<div class="alert">
    <div class="layout-wrap">
        <div class="content">
        	<?php echo $alert; ?>
            <div class="close"></div>
        </div>
    </div>
</div>
<?php endif; ?>