<?php 
global $pilot;
?>
<form id="language-toggle" action="" method="post">
	<?php wp_nonce_field('lang_nonce_save','lang_nonce'); ?>
	English
	<label class="switch">
	  <input type="checkbox" name="benefits_language" <?php if($pilot->language == "esp" ){ echo " checked"; } ?>>
	  <div class="slider"></div>
	</label>
	Espa&#241;ol
</form>